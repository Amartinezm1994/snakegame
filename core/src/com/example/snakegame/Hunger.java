package com.example.snakegame;

public interface Hunger {
	public boolean isFoodInHead(AbstractGridComponent gridFood);
	public void increaseAnimal();
}
