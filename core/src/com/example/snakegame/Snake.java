package com.example.snakegame;

import com.badlogic.gdx.utils.Array;

public class Snake implements Hunger {
	
	public static final int UP = 1;
	public static final int DOWN = 2;
	public static final int RIGHT = 3;
	public static final int LEFT = 4;
	
	private SnakeHead head;
	private Array<SnakePart> snakeBody;
	private SnakeTail tail;
	
	public Snake(int xOrig, int yOrig, int orientation) {
		head = new SnakeHead(xOrig, yOrig, orientation);
		tail = new SnakeTail(head);
		snakeBody = new Array<SnakePart>();
	}
	
	public boolean toLeft() {
		return head.setOrientation(LEFT);
	}
	public boolean toRight() {
		return head.setOrientation(RIGHT);
	}
	public boolean toUp() {
		return head.setOrientation(UP);
	}
	public boolean toDown() {
		return head.setOrientation(DOWN);
	}
	
	public void advanceOneStep() {
		head.advance();
	}
	
	public void refreshSnake() {
		if (snakeBody.size != 0) {
			snakeBody.first().advance(head);
			for (int i = 1; i < snakeBody.size; i++) {
				snakeBody.get(i).advance(snakeBody.get(i - 1));
			}
			tail.advance(snakeBody.get(snakeBody.size - 1));
		}
		else {
			tail.advance(head);
		}
	}
	
	public Array<AbstractGridComponent> getGridComponents() {
		Array<AbstractGridComponent> parts = new Array<AbstractGridComponent>();
		parts.add(head);
		if (snakeBody.size != 0)
				parts.addAll(snakeBody);
		parts.add(tail);
		return parts;
	}
	
	private void addNewBody() {
		SnakePart newBody = null;
		if (snakeBody.size == 0) {
			newBody = new SnakePart(head);
		}
		else {
			SnakePart part = snakeBody.get(snakeBody.size - 1);
			newBody = new SnakePart(part);
		}
		snakeBody.add(newBody);
	}

	@Override
	public boolean isFoodInHead(AbstractGridComponent gridFood) {
		return gridFood.getxPos() == head.getxPos() 
				&& gridFood.getyPos() == head.getyPos();
	}

	@Override
	public void increaseAnimal() {
		addNewBody();
	}
}
