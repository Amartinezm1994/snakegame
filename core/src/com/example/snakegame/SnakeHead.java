package com.example.snakegame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class SnakeHead extends SnakePart {
	
	protected static Texture HEAD_UP_TEXTURE = new Texture(Gdx.files.internal("barGreen_verticalTop.png"));
	protected static Texture HEAD_DOWN_TEXTURE = new Texture(Gdx.files.internal("barGreen_verticalBottom.png"));
	protected static Texture HEAD_LEFT_TEXTURE = new Texture(Gdx.files.internal("barGreen_horizontalLeft.png"));
	protected static Texture HEAD_RIGHT_TEXTURE = new Texture(Gdx.files.internal("barGreen_horizontalRight.png"));
	
	public SnakeHead(int xOrig, int yOrig, int orientation) {
		super(xOrig, yOrig, orientation);
		refreshHeadTexture();
		advance();
	}
	
	public void advance() {
		switch (orientation) {
		case Snake.UP:
			yPos += 1;
			break;
		case Snake.DOWN:
			yPos -= 1;
			break;
		case Snake.LEFT:
			xPos -= 1;
			break;
		case Snake.RIGHT:
			xPos += 1;
		}
		if (isCorner) {
			refreshHeadTexture();
			isCorner = false;
		}
	}
	
	private void refreshHeadTexture() {
		switch(orientation) {
		case Snake.UP:
			assocTexture = HEAD_UP_TEXTURE;
			break;
		case Snake.DOWN:
			assocTexture = HEAD_DOWN_TEXTURE;
			break;
		case Snake.LEFT:
			assocTexture = HEAD_LEFT_TEXTURE;
			break;
		case Snake.RIGHT:
			assocTexture = HEAD_RIGHT_TEXTURE;
		}
	}
	
	public boolean setOrientation(int orientation) {
		if (orientation != this.orientation) {
			boolean canRotate = false;
			switch(orientation) {
			case Snake.UP:
				if (this.orientation != Snake.DOWN)
					canRotate = true;
				break;
			case Snake.DOWN:
				if (this.orientation != Snake.UP)
					canRotate = true;
				break;
			case Snake.RIGHT:
				if (this.orientation != Snake.LEFT)
					canRotate = true;
				break;
			case Snake.LEFT:
				if (this.orientation != Snake.RIGHT)
					canRotate = true;
			}
			if (canRotate) {
				isCorner = true;
				this.orientation = orientation;
			}
			return canRotate;
		}
		return false;
	}
	
	@Override
	public void refreshTexture() {}
}
