package com.example.snakegame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class SnakeFood extends AbstractGridComponent {
	
	protected static Texture TEXTURE_FOOD = new Texture(Gdx.files.internal("iconCircle_brown.png"));

	public SnakeFood(int xPos, int yPos) {
		super(xPos, yPos);
		setAssocTexture(TEXTURE_FOOD);
	}

}
