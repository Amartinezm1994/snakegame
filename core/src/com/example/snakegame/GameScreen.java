package com.example.snakegame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen implements Screen {
	
	private Scenario scenario;
	private SpriteBatch batch;
	
	public GameScreen() {
		scenario = new Scenario(30, 30);
		batch = new SpriteBatch();
	}

	@Override
	public void show() {
		
	}

	@Override
	public void render(float delta) {
		
		batch.begin();
		scenario.draw(batch, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		batch.end();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void dispose() {
		
	}

}
