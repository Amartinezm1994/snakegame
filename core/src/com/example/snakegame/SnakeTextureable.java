package com.example.snakegame;

public interface SnakeTextureable {
	public static final int CORNER_DOWN_TEXTURE = 1;
	public static final int CORNER_UP_TEXTURE = 1;
	public static final int CORNER_LEFT_TEXTURE = 1;
	public static final int CORNER_RIGHT_TEXTURE = 1;
	
	public static final int BODY_HORIZONTAL_TEXTURE = 1;
	public static final int BODY_VERTICAL_TEXTURE = 1;
	
	public static final int HEAD_UP_TEXTURE = 1;
	public static final int HEAD_DOWN_TEXTURE = 1;
	public static final int HEAD_LEFT_TEXTURE = 1;
	public static final int HEAD_RIGHT_TEXTURE = 1;
	
	//public Texture
	
}
