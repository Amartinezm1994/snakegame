package com.example.snakegame;

import com.badlogic.gdx.graphics.Texture;

public abstract class AbstractGridComponent {
	
	protected int xPos;
	protected int yPos;
	protected Texture assocTexture;
	
	public AbstractGridComponent(int xPos, int yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	public int getxPos() {
		return xPos;
	}

	public void setxPos(int xPos) {
		this.xPos = xPos;
	}

	public int getyPos() {
		return yPos;
	}

	public void setyPos(int yPos) {
		this.yPos = yPos;
	}
	
	public Texture getAssocTexture() {
		return assocTexture;
	}

	public void setAssocTexture(Texture assocTexture) {
		this.assocTexture = assocTexture;
	}
}
