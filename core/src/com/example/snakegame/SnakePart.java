package com.example.snakegame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;

public class SnakePart extends AbstractGridComponent {
	
	protected int orientation;
	protected boolean isCorner;
	
	protected static Texture CORNER_DOWN_TEXTURE = new Texture(Gdx.files.internal("barGreen_cornerDown.png"));
	protected static Texture CORNER_UP_TEXTURE = new Texture(Gdx.files.internal("barGreen_cornerUp.png"));
	protected static Texture CORNER_LEFT_TEXTURE = new Texture(Gdx.files.internal("barGreen_cornerLeft.png"));
	protected static Texture CORNER_RIGHT_TEXTURE = new Texture(Gdx.files.internal("barGreen_cornerRight.png"));
	
	protected static Texture BODY_HORIZONTAL = new Texture(Gdx.files.internal("barGreen_horizontalMid.png"));
	protected static Texture BODY_VERTICAL = new Texture(Gdx.files.internal("barGreen_verticalMid.png"));
	
	protected SnakePart(int xOrig, int yOrig, int orientation) {
		super(xOrig, yOrig);
		this.orientation = orientation;
		isCorner = false;
	}
	public SnakePart(SnakePart partToGoBack) {
		this(partToGoBack.getxPos(), partToGoBack.getyPos(), partToGoBack.orientation);
		advance(partToGoBack);
	}

	public int getOrientation() {
		return orientation;
	}
	
	public void advance(SnakePart antPart) {
		if (isCorner) {
			isCorner = false;
			orientation = antPart.getOrientation();
		} 
		
		if (antPart.getOrientation() != orientation) {
			isCorner = true;
			setToCornerTexture(antPart.getOrientation());
		} else {
			refreshTexture();
		}
		
		switch (orientation) {
		case Snake.UP:
			if (!isCorner)
				yPos = antPart.getyPos() - 1;
			else
				yPos += 1;
			break;
		case Snake.DOWN:
			if (!isCorner)
				yPos = antPart.getyPos() + 1;
			else
				yPos -= 1;
			break;
		case Snake.LEFT:
			if (!isCorner)
				xPos = antPart.getxPos() + 1;
			else
				xPos -= 1;
			break;
		case Snake.RIGHT:
			if (!isCorner)
				xPos = antPart.getxPos() - 1;
			else
				xPos += 1;
		}
			
	}
	
	public void refreshTexture() {
		switch(orientation) {
		case Snake.UP:
		case Snake.DOWN:
			if (assocTexture != BODY_VERTICAL)
				assocTexture = BODY_VERTICAL;
			break;
		case Snake.RIGHT:
		case Snake.LEFT:
			if (assocTexture != BODY_HORIZONTAL)
				assocTexture = BODY_HORIZONTAL;
		}
	}

	public void setToCornerTexture(int refOrientation) {
		if (orientation == Snake.DOWN) {
			if (refOrientation == Snake.LEFT)
				assocTexture = CORNER_DOWN_TEXTURE;
			else
				assocTexture = CORNER_RIGHT_TEXTURE;
		} else if (orientation == Snake.UP) {
			if (refOrientation == Snake.LEFT)
				assocTexture = CORNER_LEFT_TEXTURE;
			else
				assocTexture = CORNER_UP_TEXTURE;
		} else if (orientation == Snake.RIGHT) {
			if (refOrientation == Snake.UP)
				assocTexture = CORNER_DOWN_TEXTURE;
			else
				assocTexture = CORNER_LEFT_TEXTURE;
		} else if (orientation == Snake.LEFT) {
			if (refOrientation == Snake.UP)
				assocTexture = CORNER_RIGHT_TEXTURE;
			else
				assocTexture = CORNER_UP_TEXTURE;
		}
	}

	public boolean isCorner() {
		return isCorner;
	}

}
