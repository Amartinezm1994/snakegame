package com.example.snakegame;

public class SnakeTail extends SnakePart {
	
	public SnakeTail(int xOrig, int yOrig, int orientation) {
		super(xOrig, yOrig, orientation);
		setToCornerTexture(orientation);
	}
	public SnakeTail(SnakePart part) {
		super(part);
		setToCornerTexture(orientation);
	}
	
	@Override
	public void refreshTexture() {}
	
	@Override
	public void setToCornerTexture(int refOrientation) {
		switch(refOrientation) {
		case Snake.UP:
			assocTexture = SnakeHead.HEAD_DOWN_TEXTURE;
			break;
		case Snake.DOWN:
			assocTexture = SnakeHead.HEAD_UP_TEXTURE;
			break;
		case Snake.RIGHT:
			assocTexture = SnakeHead.HEAD_LEFT_TEXTURE;
			break;
		case Snake.LEFT:
			assocTexture = SnakeHead.HEAD_RIGHT_TEXTURE;
		}
	}
}
