package com.example.snakegame;

import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

public class Scenario implements Drawable {
	
	private Snake snake;
	private Array<SnakeFood> food;
	private Hunger ham;
	
	private float leftWidth;
	private float rightWidth;
	private float topHeight;
	private float bottomHeight;
	
	private float minWidth;
	private float minHeight;
	
	private int columns;
	private int rows;
	
	public Scenario(int rows, int columns) {
		this.columns = columns;
		this.rows = rows;
		
		food = new Array<SnakeFood>();
		snake = new Snake(5, 5, Snake.RIGHT);
		for (int i = 5; i < 20; i++) {
			SnakeFood foodd = new SnakeFood(i, i);
			food.add(foodd);
		}
		ham = snake;
	}
	
	private void drawGeneric(Batch batch, float xPixel, float yPixel, float width, float height, AbstractGridComponent comp) {
		float xPos = (comp.getxPos() + 1) * width + (xPixel);
		float yPos = (comp.getyPos() + 1) * height + (yPixel);
		batch.draw(comp.getAssocTexture(), xPos, yPos, width, height);
	}
	
	@Override
	public void draw(Batch batch, float xPixel, float yPixel, float width, float height) {
		
		leftWidth = xPixel;
		rightWidth = xPixel + width;
		
		bottomHeight = yPixel;
		topHeight = yPixel + height;
		
		minWidth = width - xPixel;
		minHeight = height - yPixel;
		
		float rectWidth = width / columns;
		float rectHeight = height / rows;
		
		for (AbstractGridComponent part : snake.getGridComponents()) {
			if (part.getAssocTexture() != null) {
				drawGeneric(batch, xPixel, yPixel, rectWidth, rectHeight, part);
			}
		}

		Iterator<SnakeFood> foodIter = this.food.iterator();
		while (foodIter.hasNext()) {
			SnakeFood food = foodIter.next();
			if (ham.isFoodInHead(food)) {
				foodIter.remove();
				ham.increaseAnimal();
			}
			else drawGeneric(batch, xPixel, yPixel, rectWidth, rectHeight, food);
		}
		
		snake.advanceOneStep();
		snake.refreshSnake();
		
		
		if (Gdx.input.isKeyPressed(Input.Keys.A)) {
			snake.toLeft();
		} else if (Gdx.input.isKeyPressed(Input.Keys.D)) {
			snake.toRight();
		} else if (Gdx.input.isKeyPressed(Input.Keys.W)) {
			snake.toUp();
		} else if (Gdx.input.isKeyPressed(Input.Keys.S)) {
			snake.toDown();
		}
		
	}

	@Override
	public float getLeftWidth() {
		return leftWidth;
	}

	@Override
	public void setLeftWidth(float leftWidth) {
		this.leftWidth = leftWidth;
	}

	@Override
	public float getRightWidth() {
		return rightWidth;
	}

	@Override
	public void setRightWidth(float rightWidth) {
		this.rightWidth = rightWidth;
	}

	@Override
	public float getTopHeight() {
		return topHeight;
	}

	@Override
	public void setTopHeight(float topHeight) {
		this.topHeight = topHeight;
	}

	@Override
	public float getBottomHeight() {
		return bottomHeight;
	}

	@Override
	public void setBottomHeight(float bottomHeight) {
		this.bottomHeight = bottomHeight;
	}

	@Override
	public float getMinWidth() {
		return minWidth;
	}

	@Override
	public void setMinWidth(float minWidth) {
		this.minWidth = minWidth;
	}

	@Override
	public float getMinHeight() {
		return minHeight;
	}

	@Override
	public void setMinHeight(float minHeight) {
		this.minHeight = minHeight;
	}	
	
}
